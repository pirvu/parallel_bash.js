#!/usr/bin/env nodejs

var util = require("util");
var log_process = function(l) {
    l = Array.prototype.slice.call(l);
    if (l[0] == 'main' && argv.q) return null;
    if (l[0] == 'main') {                                                                                                                                                                                            
        l[0] = "["+l[0]+"]";                                                                                                                                                                                         
        l[1] = l[1]+":";                                                                                                                                                                                             
    } else {                                                                                                                                                                                                         
        l[0] = l[0]+":";                                                                                                                                                                                             
    }                                                                                                                                                                                                                
    if (argv.d) l.unshift(""+new Date);                                                                                                                                                                              
    return l;                                                                                                                                                                                                        
}                                                                                                                                                                                                                    
var log = function() {                                                                                                                                                                                               
    var args = log_process(arguments);
    //args = ["O"].concat(args);
    if (args) console.log.apply(console, args);                                                                                                                                                                      
};

var loge = function() {    
    var args = log_process(arguments);
    //args = ["E"].concat(args);
    if (args) console.error.apply(console, args);
};

var get_jobs_stats = function() {
    var s = {};
    s.done = 0;
    s.total = jobs.length;
    s.running = 0;
    s.ok = 0;
    s.err = 0;
    s.kill = 0;
    for (var i = 0; i<jobs.length;i++) {
        if (typeof jobs[i].r != 'undefined') {
            s.done++;
            if (jobs[i].r === 0) s.ok++;            
            else s.err++;
            
            if (jobs[i].killed) s.kill++;
        }
        else if (jobs[i].start_time) s.running++;
    }
    return s;
}

var done = function() {
    log("main", "job_stats" , get_jobs_stats());
    if (statsi) clearInterval(statsi);
    if (buff_flush_interval) clearInterval(buff_flush_interval);
    jobs.filter(function(j) { return j.r !== 0 }).map(function(j) { log("main", "failed_job", j.name, j.r, j.err) });
    var total_run_time = jobs.map(function(j) { return j.took; }).reduce(function(a,b){return a+b})/1000;
    log("main", "total_exec_time",total_run_time);
    log("main", "total_run",process.uptime());
    log("main", "parallel_spedup", Math.round(total_run_time / process.uptime()) + "X");
    var ret_code = jobs.map(function(j){return j.r}).reduce(function(a,b){return a+b});
    log("main", "sum_ret_code",ret_code);
    process.exit(ret_code);
}


var argvo = require('optimist')
    .usage('Usage: $0 [-qndsp] [-l cmd1name] [-j cmd1] [[-l cmd2name] -j cmd2 ...] [-f JOBS_FILE]')
    .default('p', require('os').cpus().length).describe('p', 'Parallel jobs')
    .default('s', 3).describe('s', 'Stats from main loop interval (0 to disable)')
    .boolean('b').describe('b', 'Buffer stdout|err')
    .default('k',0).describe('k', 'Kill after timeout (in sec)')
    .default('t',3).describe('t', 'Buffer stdout|err flush timeout')
    .default('z',1024).describe('z', 'Buffer stdout|err flush size')
    .boolean('d').describe('d', 'Prefix dates')
    .boolean('q').describe('q', 'Quiet (no messages from main loop)')
    .boolean('n').describe('n', 'Dry run (to test params parse)')
    .boolean('h').describe('h', 'Show help')
    .describe('l', 'Job label (must use for all if used)')
    //.demand('j')
    .describe('j', '* List of commands to execute')
    .describe('f', '* Json file containing jobs: [{name: JOBNAME, cmd: COMMAND}, ...]')
var argv = argvo.argv;

argv.j = typeof argv.j != "object" ? [argv.j] : argv.j;
argv.l = typeof argv.l != "object" ? (argv.l ? [argv.l] : null) : argv.l;

if (argv.h) {
    argvo.showHelp();
    process.exit(0);
}

if (argv.s > 0) 
var statsi = setInterval(function(){
    log("main", "job_stats" , get_jobs_stats())
}, 1000*argv.s);

var jobs = [];
if (argv.f) {
    argv.f = argv.f == "-" ? "/dev/stdin" : argv.f;
    log("main","read_jobs_file",argv.f);
    var jobs_string = require("fs").readFileSync(argv.f).toString();
    var jobs_raw = JSON.parse(jobs_string);
    for (var i=0;i<jobs_raw.length;i++) {
        jobs_raw[i].name = jobs_raw[i].name || "jobno" + i;
        if (jobs_raw[i].cmd) {
            log("main","push_job", jobs_raw[i].name, jobs_raw[i].cmd);
            jobs.push(jobs_raw[i]);
        }
    }
} else {
    //for (var j=0;j<50;j++) 
    for (var i=0;i<argv.j.length;i++) {
        var job = {
            name: (argv.l && argv.l[i]) || "jobno" + jobs.length,
            cmd: argv.j[i]
        };
        if (job.cmd) {
            log("main","push_job", job.name, job.cmd);
            jobs.push(job);
        }
    }
}

if (argv.n) process.exit(0);

var flush_job_buff = function(j) {
    if (j.buf_out) {            
        var buf_out = ''+j.buf_out;
        j.buf_out = '';
        if (buf_out && buf_out.length) {
            buf_out.toString().trim().split("\n").map(function(line){
                line = ''+line;
                if (line.length)
                    log(j.name, line);
            });                
        }
    }
    if (j.buf_err) {
        var buf_err = ''+j.buf_err;
        j.buf_err = '';
        if (buf_err && buf_err.length) {
            buf_err.trim().split("\n").map(function(line){
                line = ''+line;
                if (line.length)
                    loge(j.name, line);
            });       
        }
    }
}

if (argv.b)
var buff_flush_interval = setInterval(function(){
    jobs.map(flush_job_buff);
}, 1000*argv.t);

if (!jobs.length) {
    log("main","init","no_jobs");
    process.exit(0);
}
require('async').mapLimit(jobs, argv.p,
    function(task, cb) {
        log("main","starting", task.name);
        task.start_time = +new Date;        
        try {
            task.proc = require('child_process').spawn("bash", ['-c', task.cmd]);
            task.buf_out = "";
            task.buf_err = "";
            
            task.proc.stdout.on('data', function (data) {
                if (data === undefined) return;
                if (argv.b) {
                    if (!task.buf_out) task.buf_out = data.toString();
                    else task.buf_out += data.toString();
                    if (task.buf_out.length > argv.z) flush_job_buff(task);                    
                } else if (data) log(task.name, data.toString().trim());
            });
            
            task.proc.stderr.on('data', function (data) {
                if (data === undefined) return;
                if (argv.b) {
                    if (!task.buf_err) task.buf_err = data.toString();
                    else task.buf_err += data.toString();
                    if (task.buf_err.length > argv.z) flush_job_buff(task);                    
                } else if (data) loge(task.name, data.toString().trim());
            });
            
            task.proc.on('close', function (code) {
                if (task.killTo) clearTimeout(task.killTo);
                task.end_time = +new Date;
                task.took = task.end_time - task.start_time
                task.done = true;
                task.r = task.r || code;
                flush_job_buff(task);
                log("main","done_" + (task.r === 0 ? "ok":"err"), task.name, "ret_code", task.r, "in", task.took/1000, "sec", task.err || "");                
                cb();
            });
            
            if (argv.k) {
                task.killTo = setTimeout(function() {
                    if (task.done) return;
                    log("main","kill", task.name, "after", argv.k);                    
                    task.r = 255;
                    task.err = "Killed after " + argv.k + " sec";
                    task.end_time = +new Date;
                    task.took = task.end_time - task.start_time;            
                    task.proc.kill();
                    task.killed = true;
                }, 1000 * argv.k);
            }
        } catch (err) {
            task.err = err;
            task.r = 255;
            task.end_time = +new Date;
            task.took = task.end_time - task.start_time;
            log("main", "fail",task.name, err);
            cb();
        }       
    }, done);
