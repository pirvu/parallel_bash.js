About
=====

Very simple http://www.gnu.org/software/parallel/ implementation in node that fits my needs

Install dep
===========

```
npm install
```

Usage
===========

```
./parallel_bash.js 
Usage: ./node-bin/bin/node ./bin/parallel_bash.js [-qndsp] [-l cmd1name] [-j cmd1] [[-l cmd2name] -j cmd2 ...] [-f JOBS_FILE]

Options:
  -p  Parallel jobs                                                      [default: 8]
  -s  Stats from main loop interval (0 to disable)                       [default: 3]
  -b  Buffer stdout|err                                                
  -k  Kill after timeout (in sec)                                        [default: 0]
  -t  Buffer stdout|err flush timeout                                    [default: 3]
  -z  Buffer stdout|err flush size                                       [default: 1024]
  -d  Prefix dates                                                     
  -q  Quiet (no messages from main loop)                               
  -n  Dry run (to test params parse)                                   
  -h  Show help                                                        
  -l  Job label (must use for all if used)                             
  -j  * List of commands to execute                                    
  -f  * Json file containing jobs: [{name: JOBNAME, cmd: COMMAND}, ...]
```

Example
=======

```
./parallel_bash.js  -s0 -b -z3 -p20 -j "for i in \`seq 1 8\`; do echo slept $i; sleep 1; done"
```